subsystem
===========

Fork of [fabric-loader](https://github.com/FabricMC/fabric-loader) that only loads EMC and removes the fabric branding from the client so it is not visible on servers.

## License

Licensed under the Apache License 2.0.
