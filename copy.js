const fs = require('fs');
const dir = './fabric-loader/build/libs/';

fs.readdir(dir, (err, files) => {
    files.forEach(f => {
        if (f.endsWith("+local.jar")) {
            fs.copyFileSync(dir + f, "./subsystem.jar");
            console.log(`Copied ${dir}${f} to subsystem.jar`);
        }
    });
});