/*
 *  Made by Deftware 2019-09-28 
 */

const fs = require('fs');
const path = require('path');

const json = {
    "dir": "fabric-loader",
    "files": {
        "net/fabricmc/loader/launch/common/FabricLauncherBase.java": [{
                "mode": "overwrite",
                "injectionPoint": 215,
                "java": `argMap.put("accessToken", "vanilla");`
            },
            {
                "mode": "overwrite",
                "injectionPoint": 219,
                "java": `argMap.put("version", "vanilla");`
            },
            {
                "mode": "overwrite",
                "injectionPoint": 226,
                "java": `argMap.put("versionType", versionType + "vanilla");`
            }
        ],
        "net/fabricmc/loader/launch/knot/Knot.java": [{
                "mode": "add",
                "injectionPoint": 36,
                "java": `import java.nio.charset.StandardCharsets;\nimport java.nio.file.Paths;\nimport com.google.gson.*;\nimport java.nio.file.Files;\nimport org.apache.commons.io.FileUtils;\nimport javafx.application.Platform;\nimport javafx.embed.swing.JFXPanel;\nimport javafx.stage.FileChooser;`
            },
            {
                "mode": "add",
                "injectionPoint": 45,
                "java": `public static String EMCJar = "";\nprivate static String manualJsonLocation;\nprivate static boolean isFileDialogOpen = false;`
            },
            {
                "mode": "add",
                "injectionPoint": 50,
                "java": `private static String OS = System.getProperty("os.name").toLowerCase();

                public static boolean isWindows() {
                    return (OS.indexOf("win") >= 0);
                }
            
                public static boolean isMac() {
                    return (OS.indexOf("darwin") >= 0 || OS.indexOf("mac") >= 0);
                }
            
                public static boolean isLinux() {
                    return (OS.indexOf("nux") >= 0);
                }
            
                public static String getMCDir() {
                    String minecraft = "";
                    if (isWindows()) {
                        minecraft = System.getenv("APPDATA") + File.separator + ".minecraft" + File.separator;
                    } else if (isLinux()) {
                        minecraft = System.getProperty("user.home") + File.separator + ".minecraft" + File.separator;
                    } else if (isMac()) {
                        minecraft = System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support" + File.separator + "minecraft" + File.separator;
                    }
                    return minecraft;
                }
            
                public static JsonObject getJsonDataAsObject(File jsonFile) {
                    try {
                        return getJsonDataAsObject(FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8));
                    } catch (Exception ex) {
                        return null;
                    }
                }
            
                public static JsonObject getJsonDataAsObject(String jsonData) {
                    return new Gson().fromJson(jsonData, JsonObject.class);
                }
            
                public static JsonElement lookupElementInJson(File jsonFile, String searchTarget) {
                    try {
                        return lookupElementInJson(FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8), searchTarget);
                    } catch (Exception ex) {
                        return null;
                    }
                }
            
                public static JsonElement lookupElementInJson(String jsonFileData, String searchTarget) {
                    JsonElement resultingElement = null;
                    JsonObject jsonData;
            
                    try {
                        if (!jsonFileData.isEmpty()) {
                            jsonData = getJsonDataAsObject(jsonFileData);
            
                            if (jsonData != null) {
                                Set<Map.Entry<String, JsonElement>> entries = jsonData.entrySet();
                                for (Map.Entry<String, JsonElement> entry: entries) {
                                    if (entry.getKey().contains(searchTarget)) {
                                        resultingElement = entry.getValue();
                                    }
                                }
                            } else {
                                System.out.println("Json Data returned null, looking for " + searchTarget);
                            }
                        } else {
                            System.out.println("Json File Data is invalid, please correct your parameters!");
                        }
                    } catch (Exception ex) {
                        System.out.println("Failed to lookup " + searchTarget + " in json file...");
                        ex.printStackTrace();;
                    }
            
                    return resultingElement;
                }
            
                public static File getEMCJsonFile(String attemptedFileLocation) throws InterruptedException {
                    File jsonFile = new File(attemptedFileLocation);
            
                    if (!jsonFile.exists()) {
                        if (manualJsonLocation != null && new File(manualJsonLocation).exists()) {
                            return new File(manualJsonLocation);
                        } else {
                            System.out.println("Opening File Open Dialog, as JSON Cannot be found");
            
                            JFXPanel frame = new JFXPanel(); // Initialize JavaFX Environment
                            isFileDialogOpen = true;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    FileChooser fileChooser = new FileChooser();
                                    fileChooser.setTitle("Open EMC Json File (Required)");
                                    fileChooser.getExtensionFilters().addAll(
                                        new FileChooser.ExtensionFilter("Json", "*.json")
                                    );
            
                                    File resultFile = fileChooser.showOpenDialog(null);
            
                                    if (resultFile != null) {
                                        manualJsonLocation = resultFile.getAbsolutePath();
                                    } else {
                                        System.out.println("JSON not found, things will break if other addons are using this!");
                                    }
                                    isFileDialogOpen = false;
                                }
                            });
                        }
                    } else {
                        return jsonFile;
                    }
            
                    while (isFileDialogOpen) {
                        Thread.sleep(1000);
                    }
            
                    return new File(manualJsonLocation);
                }`
            },
            {
                "mode": "add",
                "injectionPoint": 52,
                "java": `File initFile = new File(getMCDir() + ".subinit");
                if (!initFile.exists()) {
                    File fabric = new File(getMCDir() + ".fabric" + File.separator);
                    if (fabric.exists()) {
                        try {
                            Files.walk(fabric.toPath(), new java.nio.file.FileVisitOption[0])
                                .sorted(Comparator.reverseOrder())
                                .map(Path::toFile)
                                .forEach(File::delete);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    try {
                        initFile.createNewFile();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }`
            },
            {
                "mode": "add",
                "injectionPoint": 104,
                "java": `for (Path path : provider.getGameContextJars()) {
                    if (!path.getFileName().toString().contains("realms")) {
                        String gameDir = ((this.provider.getLaunchDirectory().toString().toLowerCase().contains("multimc") ? getMCDir() : this.provider.getLaunchDirectory().toString() + File.separator)),
                            jsonPath = gameDir + "versions" + File.separator + path.getFileName().toString().replace(".jar", "") + File.separator + path.getFileName().toString().replace(".jar", "") + ".json";
                        try {
                            File jsonFile = getEMCJsonFile(jsonPath);
        
                            if (!jsonFile.exists()) {
                                System.err.println("Could not find json file!");
                            } else {
                                JsonElement libraries = lookupElementInJson(jsonFile, "libraries");
                                if (libraries != null) {
                                    JsonArray array = libraries.getAsJsonArray();
                                    array.forEach(jsonElement -> {
                                        JsonObject entry = jsonElement.getAsJsonObject();
                                        if (entry.get("name").getAsString().contains("EMC-F")) {
                                            String[] current = entry.get("name").getAsString().split(":");
                                            EMCJar = gameDir + "libraries" + File.separator + "me" + File.separator + "deftware" + File.separator + "EMC-F" + File.separator + current[2];
                                        }
                                    });
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        break;
                    }
                }`
            }
        ],
        "net/fabricmc/loader/FabricLoader.java": [{
                "mode": "overwrite",
                "injectionPoint": 96,
                "java": `this.gameDir = gameDir.getName().toLowerCase().contains("multimc") ? new File(Knot.getMCDir()) : gameDir;`
            },
            {
                "mode": "overwrite",
                "injectionPoint": 130,
                "java": `return new File(Knot.EMCJar);`
            },
            {
                "mode": "overwrite",
                "injectionPoint": 175,
                "java": `LOGGER.info("Loading EMC... (" + candidateMap.size() + ")");`
            },
            {
                "mode": "overwrite",
                "injectionPoint": 176,
                "java": ``
            },
            {
                "mode": "overwrite",
                "injectionPoint": 177,
                "java": ``
            }
        ]
    },
    "replace": [{
            "find": "\"Fabric\|Loader\"",
            "replace": "\"Subsystem\""
        },
        {
            "find": "\"Fabric\|Tweaker\"",
            "replace": "\"Subsystem\""
        },
        {
            "find": "\"Fabric\|MixinBootstrap\"",
            "replace": "\"Subsystem\""
        },
        {
            "find": "\"FabricLoader\"",
            "replace": "\"Subsystem\""
        },
        {
            "find": "\"FabricLoader\|EntrypointTransformer\"",
            "replace": "\"Subsystem\""
        }
    ]
};

async function* getFiles(dir) {
    const dirents = await fs.promises.readdir(dir, {
        withFileTypes: true
    });
    for (const dirent of dirents) {
        const res = path.resolve(dir, dirent.name);
        if (dirent.isDirectory()) {
            yield* getFiles(res);
        } else {
            yield res;
        }
    }
}

(async () => {
    for await (let file of getFiles(`./${json.dir}/`)) {
        if (file.endsWith(".java") && file.includes(`src${path.sep}main`)) {
            let contents = fs.readFileSync(file).toString().split("\n");
            file = `.${path.sep}${json.dir}${file.split(json.dir)[1]}`;
            Object.keys(json.files).forEach(k => {
                if (file.includes(k.replace(/\//g, path.sep))) {
                    let pointOffset = 0;
                    json.files[k].forEach(patch => {
                        switch (patch.mode) {
                            case "add":
                                patch.java = patch.java.split("\n").reverse();
                                for (let i in patch.java) {
                                    contents.splice(patch.injectionPoint - 1 + pointOffset, 0, patch.java[i]);
                                }
                                pointOffset += patch.java.length;
                                break;
                            case "overwrite":
                                contents[patch.injectionPoint - 1 + pointOffset] = patch.java;
                                break;
                            default:
                                console.error("Unknown injection mode");
                        }
                    });
                }
            });
            json.replace.forEach(entry => {
                for (let i in contents) {
                    contents[i] = contents[i].replace(entry.find, entry.replace);
                }
            });
            fs.writeFileSync(file, contents.join("\n"));
        }
    }
})();